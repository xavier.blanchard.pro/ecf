<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211029160902 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE chambre_froide ADD officine_id INT NOT NULL');
        $this->addSql('ALTER TABLE chambre_froide ADD CONSTRAINT FK_5E245AA7B2D03E4E FOREIGN KEY (officine_id) REFERENCES officine (id)');
        $this->addSql('CREATE INDEX IDX_5E245AA7B2D03E4E ON chambre_froide (officine_id)');
        $this->addSql('ALTER TABLE data_hygro ADD chambre_froide_id INT NOT NULL');
        $this->addSql('ALTER TABLE data_hygro ADD CONSTRAINT FK_BA745200C621DF84 FOREIGN KEY (chambre_froide_id) REFERENCES chambre_froide (id)');
        $this->addSql('CREATE INDEX IDX_BA745200C621DF84 ON data_hygro (chambre_froide_id)');
        $this->addSql('ALTER TABLE data_temp ADD chambre_froide_id INT NOT NULL');
        $this->addSql('ALTER TABLE data_temp ADD CONSTRAINT FK_B01C4E28C621DF84 FOREIGN KEY (chambre_froide_id) REFERENCES chambre_froide (id)');
        $this->addSql('CREATE INDEX IDX_B01C4E28C621DF84 ON data_temp (chambre_froide_id)');
        $this->addSql('ALTER TABLE officine ADD user_id INT NOT NULL');
        $this->addSql('ALTER TABLE officine ADD CONSTRAINT FK_66339666A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_66339666A76ED395 ON officine (user_id)');
        $this->addSql('ALTER TABLE validation ADD chambre_froide_id INT NOT NULL');
        $this->addSql('ALTER TABLE validation ADD CONSTRAINT FK_16AC5B6EC621DF84 FOREIGN KEY (chambre_froide_id) REFERENCES chambre_froide (id)');
        $this->addSql('CREATE INDEX IDX_16AC5B6EC621DF84 ON validation (chambre_froide_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE chambre_froide DROP FOREIGN KEY FK_5E245AA7B2D03E4E');
        $this->addSql('DROP INDEX IDX_5E245AA7B2D03E4E ON chambre_froide');
        $this->addSql('ALTER TABLE chambre_froide DROP officine_id');
        $this->addSql('ALTER TABLE data_hygro DROP FOREIGN KEY FK_BA745200C621DF84');
        $this->addSql('DROP INDEX IDX_BA745200C621DF84 ON data_hygro');
        $this->addSql('ALTER TABLE data_hygro DROP chambre_froide_id');
        $this->addSql('ALTER TABLE data_temp DROP FOREIGN KEY FK_B01C4E28C621DF84');
        $this->addSql('DROP INDEX IDX_B01C4E28C621DF84 ON data_temp');
        $this->addSql('ALTER TABLE data_temp DROP chambre_froide_id');
        $this->addSql('ALTER TABLE officine DROP FOREIGN KEY FK_66339666A76ED395');
        $this->addSql('DROP INDEX UNIQ_66339666A76ED395 ON officine');
        $this->addSql('ALTER TABLE officine DROP user_id');
        $this->addSql('ALTER TABLE validation DROP FOREIGN KEY FK_16AC5B6EC621DF84');
        $this->addSql('DROP INDEX IDX_16AC5B6EC621DF84 ON validation');
        $this->addSql('ALTER TABLE validation DROP chambre_froide_id');
    }
}
