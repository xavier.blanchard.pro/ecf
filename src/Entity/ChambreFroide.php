<?php

namespace App\Entity;

use App\Repository\ChambreFroideRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ChambreFroideRepository::class)
 */
class ChambreFroide
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Nom;

    /**
     * @ORM\Column(type="decimal", precision=4, scale=2)
     */
    private $TempMin;

    /**
     * @ORM\Column(type="decimal", precision=4, scale=2)
     */
    private $TempMax;

    /**
     * @ORM\Column(type="integer")
     */
    private $HygroMin;

    /**
     * @ORM\Column(type="integer")
     */
    private $HygroMax;

    /**
     * @ORM\ManyToOne(targetEntity=Officine::class, inversedBy="ChambreFroides")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Officine;

    /**
     * @ORM\OneToMany(targetEntity=DataTemp::class, mappedBy="ChambreFroide")
     */
    private $DataTemps;

    /**
     * @ORM\OneToMany(targetEntity=DataHygro::class, mappedBy="ChambreFroide")
     */
    private $DataHygros;

    /**
     * @ORM\OneToMany(targetEntity=Validation::class, mappedBy="ChambreFroide")
     * @ORM\JoinColumn(nullable=true)
     */
    private $Validations;

    public function __construct()
    {
        $this->DataTemps = new ArrayCollection();
        $this->DataHygros = new ArrayCollection();
        $this->Validations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->Nom;
    }

    public function setNom(string $Nom): self
    {
        $this->Nom = $Nom;

        return $this;
    }

    public function getTempMin(): ?string
    {
        return $this->TempMin;
    }

    public function setTempMin(string $TempMin): self
    {
        $this->TempMin = $TempMin;

        return $this;
    }

    public function getTempMax(): ?string
    {
        return $this->TempMax;
    }

    public function setTempMax(string $TempMax): self
    {
        $this->TempMax = $TempMax;

        return $this;
    }

    public function getHygroMin(): ?int
    {
        return $this->HygroMin;
    }

    public function setHygroMin(int $HygroMin): self
    {
        $this->HygroMin = $HygroMin;

        return $this;
    }

    public function getHygroMax(): ?int
    {
        return $this->HygroMax;
    }

    public function setHygroMax(int $HygroMax): self
    {
        $this->HygroMax = $HygroMax;

        return $this;
    }

    public function getOfficine(): ?Officine
    {
        return $this->Officine;
    }

    public function setOfficine(?Officine $Officine): self
    {
        $this->Officine = $Officine;

        return $this;
    }

    /**
     * @return Collection|DataTemp[]
     */
    public function getDataTemps(): Collection
    {
        return $this->DataTemps;
    }

    public function addDataTemp(DataTemp $dataTemp): self
    {
        if (!$this->DataTemps->contains($dataTemp)) {
            $this->DataTemps[] = $dataTemp;
            $dataTemp->setChambreFroide($this);
        }

        return $this;
    }

    public function removeDataTemp(DataTemp $dataTemp): self
    {
        if ($this->DataTemps->removeElement($dataTemp)) {
            // set the owning side to null (unless already changed)
            if ($dataTemp->getChambreFroide() === $this) {
                $dataTemp->setChambreFroide(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DataHygro[]
     */
    public function getDataHygros(): Collection
    {
        return $this->DataHygros;
    }

    public function addDataHygro(DataHygro $dataHygro): self
    {
        if (!$this->DataHygros->contains($dataHygro)) {
            $this->DataHygros[] = $dataHygro;
            $dataHygro->setChambreFroide($this);
        }

        return $this;
    }

    public function removeDataHygro(DataHygro $dataHygro): self
    {
        if ($this->DataHygros->removeElement($dataHygro)) {
            // set the owning side to null (unless already changed)
            if ($dataHygro->getChambreFroide() === $this) {
                $dataHygro->setChambreFroide(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Validation[]
     */
    public function getValidations(): Collection
    {
        return $this->Validations;
    }

    public function addValidation(Validation $validation): self
    {
        if (!$this->Validations->contains($validation)) {
            $this->Validations[] = $validation;
            $validation->setChambreFroide($this);
        }

        return $this;
    }

    public function removeValidation(Validation $validation): self
    {
        if ($this->Validations->removeElement($validation)) {
            // set the owning side to null (unless already changed)
            if ($validation->getChambreFroide() === $this) {
                $validation->setChambreFroide(null);
            }
        }

        return $this;
    }
}
