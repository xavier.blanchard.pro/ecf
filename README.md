# ECF-STUDI-RDtemp

Projet ECF du Graduate Développeur Web et Web Mobile STUDI

## Environement de développement

### Pré-requis

* PHP 7.4
* Composer
* Symfony CLI
* LAMPP pour base MySQL/MariaDB
* Bootstrap (twbs/bootstrap:5.0.2)
* Doctrine
* MakerBundle (symfony/maker-bundle --dev)
* ORM-Fixtures (orm-fixtures --dev)
* FakerPHP (fakerphp/faker --dev)
* Security-Bundle (symfony/security-bundle)
* PasswordHasher (symfony/password-hasher)


Vous pouvez vérifier les pré-requis avec la commande suivante de la CLI-Symfony :

***bash
symfony check:requirements
***

### Lancer l'environnement de développement

composer install
symfony serve -d

### Lancer les test unitaires

***bash
php bin/phpunit --testdox
***

# Credits

Xavier BLANCHARD - xavier.blanchard.pro@gmail.com